/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inserçao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author aluno
 */
public class cad {
    public void Inserir ( int cod, String nomecadeia, String endereco) throws ClassNotFoundException, SQLException{
        Connection c = FabricaConexao.obterConexao();
        try (PreparedStatement p = c.prepareStatement("insert into sc_crime.tb_cadeias(cod,nomecadeia,endereco) values (?,?,?);")) {
            p.setInt(1,cod);
            p.setString(2,nomecadeia);
            p.setString(3,endereco);
            p.execute();
            p.close();
        }
    }
    
     public void update ( int cod, String nomecadeia, String endereco) throws ClassNotFoundException, SQLException{
        Connection c = FabricaConexao.obterConexao();
        PreparedStatement p =c.prepareStatement("update sc_crime.tb_cadeias set nomecadeia = ? , endereco = ?  where cod = ?;");
                p.setString(1,nomecadeia);
                p.setString(2,endereco);
                p.setInt(3,cod);
                p.execute();
                p.close();
    }
     public void delete ( int cod, String nomecadeia, String endereco) throws ClassNotFoundException, SQLException{
        Connection c = FabricaConexao.obterConexao();
        try (PreparedStatement p = c.prepareStatement("delete from sc_crime.tb_cadeias where cod = ?;")) {
            p.setInt(1,cod);
            p.execute();
        }
    }
    public void Exibir (JList lista) throws ClassNotFoundException, SQLException{
        DefaultListModel m = new DefaultListModel();
        Connection c = FabricaConexao.obterConexaoPostgreeSQL();
        String SQL = "Select cod,nome,endereco FROM sc_crime.tb_cadeias";
        PreparedStatement p = c.prepareStatement(SQL); 
        ResultSet r = p.executeQuery();
        while(r.next()){
                    m.addElement(r.getInt("cod")+" / "+r.getString("nomecadeia")+" / "+r.getString("endereco"));      
        }
        lista.setModel(m);
        
    }
}
