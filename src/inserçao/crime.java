package inserçao;

import java.sql.PreparedStatement;
import inserçao.FabricaConexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import jdk.nashorn.internal.runtime.ListAdapter;

public class crime {
    public void Inserir (String nome, String cod, String crime) throws ClassNotFoundException, SQLException{
        Connection c = FabricaConexao.obterConexao();
        try (PreparedStatement p = c.prepareStatement("insert into sc_crime.presos(nome,cod,crime) values (?,?,?);")) {
            p.setString(1,nome);
            p.setString(2,cod);
            p.setString(3,crime);
            p.execute();
            p.close();
        }
    }
    
     public void update (String nome, String cod, String crime) throws ClassNotFoundException, SQLException{
        Connection c = FabricaConexao.obterConexao();
        PreparedStatement p =c.prepareStatement("update sc_crime.presos set nome = ? , crime = ?  where cod = ?;");
                p.setString(1,nome);
                p.setString(2,crime);
                p.setString(3,cod);
                p.execute();
                p.close();
    }
     public void delete (String nome, String cod, String crime) throws ClassNotFoundException, SQLException{
        Connection c = FabricaConexao.obterConexao();
        try (PreparedStatement p = c.prepareStatement("delete from sc_crime.presos  where cod = ?;")) {
            p.setString(1,cod);
            p.execute();
        }
    }
    public void Exibir (JList lista) throws ClassNotFoundException, SQLException{
        DefaultListModel m = new DefaultListModel();
        Connection c = FabricaConexao.obterConexaoPostgreeSQL();
        String SQL = "Select nome,cod,crime FROM sc_crime.presos";
        PreparedStatement p = c.prepareStatement(SQL); 
        ResultSet r = p.executeQuery();
        while(r.next()){
                    m.addElement(r.getString("nome")+" / "+r.getString("cod")+" / "+r.getString("crime"));      
        }
        lista.setModel(m);
        
    }
    
  
}
