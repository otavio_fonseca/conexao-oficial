/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inserçao;


import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author aluno
 */
public class tipocrime {
  public void inserirtipo (String tipo, String id_crime, String ex) throws ClassNotFoundException, SQLException{
        Connection c = FabricaConexao.obterConexaoPostgreeSQL();
        try (PreparedStatement f = c.prepareStatement("insert into sc_crime.tipocrime(tipo,id_crime,ex) values (?,?,?);")) {
            f.setString(1,tipo);
            f.setString(2,id_crime);
            f.setString(3,ex);
            f.execute();
            f.close();
         
        }
        }  
}
